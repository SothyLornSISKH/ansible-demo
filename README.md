```bash
ansible-galaxy init ansible/roles/nginx  
```

```bash
ansible-playbook ansible/configure.yml -i hosts -l dev
```

```bash
ansible-playbook ansible/docker.yml -i hosts -l dev
```